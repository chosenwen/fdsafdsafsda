#!/usr/bin/env python

import smach
import sys
import rospy
import time
import tf
from mascor_px4_control import MAV
from geometry_msgs.msg import Point
from std_msgs.msg import String
from geometry_msgs.msg import PoseStamped

# create MAV objecti

def callback_loc_pose(poseStamped):	
	global current_z, current_x, current_y
	current_x = poseStamped.pose.position.x
	current_y = poseStamped.pose.position.y   
	current_z = poseStamped.pose.position.z

def callback_target(point):
	global given_target
	given_target = point
	
def callback_checkbox(checkbox):
	global camera_state 
	camera_state = checkbox.data

def callback_compensation(data):
	global compensation
	compensation = data.data

current = Point()

if __name__ == '__main__':
	# define Subscribers 
	rospy.Subscriber('/uav2/checkbox_camera', String, callback_checkbox)
	rospy.Subscriber('/uav2/compensation', String, callback_compensation)
	rospy.Subscriber('/uav2/mavros/local_position/pose', PoseStamped, callback_loc_pose)
	rospy.Subscriber('/uav2/target', Point, callback_target)

	# define Publisher
	pub_checkaction = rospy.Publisher('/uav2/checkbox_action', String, queue_size=10)
	
	# initial node
	rospy.init_node('UAV_control', anonymous=True)
	rate = rospy.Rate(25)
	listener = tf.TransformListener()
	
	# define UAV
	global multicopter1
	multicopter1 = MAV("/uav2", "multicopter")
	multicopter1.arm()
	global current_x, current_y, current_z
	global camera_state, uav_state, compensation
	global given_target, local_target
	camera_state = " "
	uav_state = " "
	compensation = " "
	given_target = Point()
	local_target = Point()
	
	# Give init point
	rospy.loginfo('Executing state INIT') 
	multicopter1.setpoint_pos([22.5, -25, 20])
	uav_state = "init"

	while not rospy.is_shutdown():
		print "x, y, z = %s, %s, %s" %(current_x, current_y, current_z)
		print uav_state
		if (uav_state == "init"):
			if (abs(abs(current_x)-20) < 0.5 and abs(abs(current_y)-25) < 0.5):	
				uav_state = "estimate_point"
				pub_checkaction.publish("estimate_point")
		if (camera_state == "estimate_end"):
			uav_state = "gime_target"
			pub_checkaction.publish(uav_state)
		elif camera_state == "given_target":
			local_target.x = given_target.x
			local_target.y = given_target.y
			local_target.z = 8
			multicopter1.setpoint_pos([local_target.x, local_target.y, local_target.z])
			while (abs(abs(current_x) - abs(local_target.x)) > 1 or abs(abs(current_y) - abs(local_target.y)) > 1 or abs(abs(current_z)-abs(local_target.z)>1)):
				print "Haven't arrived"
				print "Target: (%s, %s, %s)"%(local_target.x, local_target.y, local_target.z)
				print "Current: (%s, %s, %s)" %(current_x, current_y, current_z)
			uav_state = "check"
			pub_checkaction.publish(uav_state)
		elif camera_state == "given_compensation":
			print "Target: (%s, %s, %s)"%(local_target.x, local_target.y, local_target.z)
			uav_state = "get the compensation"
			if current_z > 5:
				correction = 0.025
			elif current_z > 3:
				correction = 0.001

			if compensation == "ul":
				local_target.x = local_target.x - correction 
				local_target.y = local_target.y + correction 
			elif compensation == "ur":
				local_target.x = local_target.x + correction 
				local_target.y = local_target.y + correction 
			elif compensation == "dl":
				local_target.x = local_target.x - correction 
				local_target.y = local_target.y - correction 
			elif compensation == "dr":
				local_target.x = local_target.x + correction 
				local_target.y = local_target.y - correction 
			elif compensation == "d":
				local_target.x = local_target.x
				local_target.y = local_target.y - correction 
			elif compensation == "u":
				local_target.x = local_target.x
				local_target.y = local_target.y + correction 
			elif compensation == "l":
				local_target.x = local_target.x - correction 
				local_target.y = local_target.y 
			elif compensation == "r":
				local_target.x = local_target.x + correction 
				local_target.y = local_target.y
			elif compensation == "down":
				if local_target.z > 3:
					local_target.z = local_target.z - 2
				else:
					uav_state = "direct_go_down"
					local_target.z = 0
			# print "Target: (%s, %s, %s)"%(local_target.x, local_target.y, local_target.z)
			multicopter1.setpoint_pos([local_target.x, local_target.y, local_target.z])
			while ((not rospy.is_shutdown()) and (abs(abs(current_x) - abs(local_target.x)) > 1 or abs(abs(current_y) - abs(local_target.y)) > 1 or abs(abs(current_z)-abs(local_target.z)>1))):
				print "Haven't arrived"
				print "Target: (%s, %s, %s)"%(local_target.x, local_target.y, local_target.z)
				print "Current: (%s, %s, %s)" %(current_x, current_y, current_z)
			if current_z < 0.5:
				uav_state = "gime_target"
				pub_checkaction.publish(uav_state)
	
		rate.sleep()
