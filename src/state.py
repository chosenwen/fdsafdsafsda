#!/usr/bin/env python

import smach
import sys
import rospy
import time
import tf
from mascor_px4_control import MAV
from geometry_msgs.msg import Point
from std_msgs.msg import String

# define state Init
class Init(smach.State):
   
    def __init__(self):
        smach.State.__init__(self, outcomes=['outcome1'])
        
    def execute(self, userdata):
                
        rospy.loginfo('Executing state INIT') 
        
        
        multicopter1.setpoint_pos([20, -25, 20])
        rospy.sleep(10.)
       
        return 'outcome1'       

# define state Detect
class Detect(smach.State):
    
    def __init__(self):
        smach.State.__init__(self, outcomes=['outcome2'])

    def execute(self, userdata):
        
        rospy.loginfo('Executing state Detect')
        rospy.Subscriber('/uav2/target', Point, callback)
        rospy.sleep(15.)
                 
        return 'outcome2'

# define state Up
class Up(smach.State):
    
    def __init__(self):
        smach.State.__init__(self, outcomes=['outcome3'])

    def execute(self, userdata):
        
        rospy.loginfo('Executing state UP')

        pub = rospy.Publisher('/uav2/checkbox', String, queue_size=10)
        pub.publish("empty")

        multicopter1.setpoint_pos([x_pos, y_pos, z_pos])
        rospy.sleep(20.)
        
        return 'outcome3'



# main
def main():

    rospy.init_node('mascorPX4controlTest', anonymous=True)
    rate = rospy.Rate(25)
    listener = tf.TransformListener()

    ## create MAV object
    global multicopter1
    multicopter1 = MAV("/uav2", "multicopter")
    multicopter1.arm()

    # Create a SMACH state machine
    sm = smach.StateMachine(outcomes=['outcome4', 'outcome5'])

    # Open the container
    with sm:
        # Add states to the container
        smach.StateMachine.add('INIT', Init(), 
                               transitions={'outcome1':'DETECT'})
        smach.StateMachine.add('DETECT', Detect(), 
                               transitions={'outcome2':'UP'})
        smach.StateMachine.add('UP', Up(), 
                               transitions={'outcome3':'DETECT'})

    # Execute SMACH plan
    outcome = sm.execute()

def callback(data):
        #print data.x
        #print data.y
        #print data.z
        global x_pos
        global y_pos
        global z_pos
        x_pos = data.x
        y_pos = data.y
        z_pos = 5

if __name__ == '__main__':
    main()
