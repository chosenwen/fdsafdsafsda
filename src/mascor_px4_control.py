#!/usr/bin/env python
# -*- coding: latin-1 -*-

####
## TODO
## - sub-classes: Queue, Transforms, 
## - correct multithreading implementation
## - 
##
##
####

import thread
import threading
import math
import numpy

import time

import rospy
import tf
import tf2_ros

# Messages
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import TransformStamped
from geometry_msgs.msg import Transform
from sensor_msgs.msg import NavSatFix
from mavros_msgs.msg import State
from mavros_msgs.msg import ExtendedState
from mavros_msgs.msg import PositionTarget
# Services
from mavros_msgs.srv import CommandBool
from mavros_msgs.srv import CommandLong
from mavros_msgs.srv import CommandTOL
from mavros_msgs.srv import SetMode


class MAV:
    mavCount = 0

    def __init__(self, namespace, modeltype, framespublisher=True):
        MAV.mavCount += 1
        self.__lock = threading.Lock()
        self.__threadkiller = False
        self.__namespace = namespace
        self.__type = modeltype
        self.__id = MAV.mavCount
        self.__idString = "MAV " + str(self.__id) + ": " 
        self.__state = State()
        self.__extState = ExtendedState()
        self.__armed = False
        self.__flightmode = "MANUAL"
        self.__lastTransform = TransformStamped()
        self.__lastTransform.transform.translation.x = 0
        self.__lastTransform.transform.translation.y = 0
        self.__lastTransform.transform.translation.z = 0
        self.__safety_x1 = -75.0
        self.__safety_y1 = -75.0
        self.__safety_z1 = -1
        self.__safety_x2 = 75.0
        self.__safety_y2 = 75.0
        self.__safety_z2 = 35.0
        self.__offset = False
        self.__local_x = 0.0
        self.__local_y = 0.0
        self.__local_z = 0.0
        self.__local_offset_x = 0.0
        self.__local_offset_y = 0.0
        self.__local_offset_z = 0.0
        self.__local_offset = [0.0, 0.0, 0.0]
        self.__local_pos = [0.0, 0.0, 0.0]
        self.__local_quat = [0.0, 0.0, 0.0, 1.0]
        self.__global_x = 0.0
        self.__global_y = 0.0
        self.__global_z = 0.0
        self.__gpsFix = False
        self.__latitude = 0.0
        self.__longitude = 0.0
        self.__altitude = 0.0
        self.__setpoint_mask = 4088
        self.__setpoint_x = 0.0
        self.__setpoint_y = 0.0
        self.__setpoint_z = 0.0
        self.__setpoint_vx = 0.0
        self.__setpoint_vy = 0.0
        self.__setpoint_vz = 0.0
        self.__setpoint_yaw = 0.0
        self.__setpoint_accuracy = 0.5
        self.__goto = [0.0, 0.0, 0.0]
        self.__goto_queue = []
        self.__queuePointer = 0
        self.__queue_active = False

        self.__rate1 = rospy.Rate(1)
        self.__rate5 = rospy.Rate(5)
        self.__rate25 = rospy.Rate(25)
        

        # TF
        self.__tfBuffer = tf2_ros.Buffer()
        self.__tfListener = tf2_ros.TransformListener(self.__tfBuffer)
        self._br = tf2_ros.TransformBroadcaster()

        # Subscriber
        self.localPose_sub = rospy.Subscriber(self.__namespace + '/mavros/local_position/pose', PoseStamped, self.__cbLocPose)
        self.globalPose_sub = rospy.Subscriber(self.__namespace + '/mavros/global_position/global', NavSatFix, self.__cbGlobPose)
        self.state_sub = rospy.Subscriber(self.__namespace + '/mavros/state', State, self.__cbState)
        self.extstate_sub = rospy.Subscriber(self.__namespace + '/mavros/extended_state', ExtendedState, self.__cbExtState)
        # Publisher
        self.setpointRaw_pub = rospy.Publisher(self.__namespace + '/mavros/setpoint_raw/local', PositionTarget, queue_size = 1)

        # Init
        # wait until connection with FCU 
        while not rospy.is_shutdown() and not self.__state.connected:
            rospy.Rate(20)     
        self.loginfo('[FCU connection] ✓')
        while not self.preflightcheck():
            self.__rate1.sleep()

        #Thread
        try:
            thread.start_new_thread(self.__watchdog, ())
        except:
            self.logerr("Error: Unable to start thread 'watchdog'")
        
        try:
            thread.start_new_thread(self.__setpointRaw, ())
        except:
            self.logerr("Error: Unable to start thread 'setpointRAW'")
        if framespublisher:    
            try:
                thread.start_new_thread(self.__framespublisher, ())
            except:
                self.logerr("Error: Unable to start thread 'framespublisher'")


    def delete(self):
        if self.__extState.landed_state == 1:
            self.__threadkiller = True
            s = self.__idString + "[DELETE] ✓"
            self.loginfo(s)
            MAV.mavCount -= 1
            return 1
        else:
            s = self.__idString + "[DELETE] ❌"
            self.loginfo(s)
            return 0


    def loginfo(self, string):
        rospy.loginfo(self.createString(string))

    def logwarn(self, string):
        rospy.logwarn(self.createString(string))

    def logerr(self, string):
        rospy.logerr(self.createString(string))

    def createString(self, string):
        s = self.__idString + string
        return s


    def __cbLocPose(self, localPose):
        self.__local_x = localPose.pose.position.x
        self.__local_y = localPose.pose.position.y
        self.__local_z = localPose.pose.position.z
        self.__local_qx = localPose.pose.orientation.x
        self.__local_qy = localPose.pose.orientation.y
        self.__local_qz = localPose.pose.orientation.z
        self.__local_qw = localPose.pose.orientation.w
        self.__local_pos = [self.__local_x, self.__local_y, self.__local_z]
        self.__local_quat = [self.__local_qx, self.__local_qy, self.__local_qz, self.__local_qw]


    def __cbGlobPose(self, globalPose):
        if not self.__gpsFix:
            if not self.__latitude == 0.0:
                self.__gpsFix = True
                self.loginfo("[GPS] ✓")
        self.__latitude = globalPose.latitude
        self.__longitude = globalPose.longitude
        self.__altitude = globalPose.altitude


    def __cbState(self, state):
        self.__state = state
        if self.__armed != state.armed:
            self.__armed = state.armed
            if state.armed == True:
                self.loginfo("[ARMED]")
            else:
                self.loginfo("[DISARMED]")
        if self.__flightmode != state.mode:
            self.__flightmode = state.mode
            s = "[FLIGHTMODE] " + str(state.mode)
            self.loginfo(s)


    def __cbExtState(self, state):
        self.__extState = state


    def __setLocalOffset(self):
        self.loginfo("[OFFSET EST.] ...")

        std_list_x = []
        std_list_y = []
        std_list_z = []
        euler = [0.0, 0.0, 0.0]
        quat = [0.0, 0.0, 0.0, 1.0]
        while not self.__offset:
            for i in range(1,100,1):
                std_list_x.append(self.__local_x)
                std_list_y.append(self.__local_y)
                std_list_z.append(self.__local_z)
                self.__rate25.sleep()
            std_x = numpy.std(std_list_x)
            std_y = numpy.std(std_list_y)
            std_z = numpy.std(std_list_z)
            if (std_x < 0.05 and std_y < 0.05 and std_z < 0.5):
                self.loginfo("[OFFSET EST.] ✓")
                self.__offset = True
        self.__local_offset_x = numpy.mean(std_list_x)
        self.__local_offset_y = numpy.mean(std_list_y)
        self.__local_offset_z = numpy.mean(std_list_z)
        self.__local_offset = (self.__local_offset_x, self.__local_offset_y, self.__local_offset_z)
        euler = tf.transformations.euler_from_quaternion(self.__local_quat)
        quat = tf.transformations.quaternion_from_euler(-euler[0], -euler[1], euler[2])
        self.__local_quat_offset = (quat)
        self.setSafetyZone(self.__safety_x1, self.__safety_y1, self.__safety_z1, self.__safety_x2, self.__safety_y2, self.__safety_z2)


    def __watchdog(self):
        while not self.__threadkiller:
            if not self.mavInSafetyZone():
                self.returnToHome()
                self.__rate5.sleep()


    def __setpointRaw(self):
        raw_msg = PositionTarget()
        raw_msg.header.frame_id = "home"
        while not self.__threadkiller:
            if self.__queue_active:
                i = self.__queuePointer


            raw_msg.header.stamp = rospy.Time.now()
            raw_msg.coordinate_frame = 1
            raw_msg.type_mask  = self.__setpoint_mask
            raw_msg.position.x = self.__setpoint_x
            raw_msg.position.y = self.__setpoint_y
            raw_msg.position.z = self.__setpoint_z
            raw_msg.velocity.x = self.__setpoint_vx
            raw_msg.velocity.y = self.__setpoint_vy
            raw_msg.velocity.z = self.__setpoint_vz
            raw_msg.yaw        = self.__setpoint_yaw
            self.setpointRaw_pub.publish(raw_msg)
            self.__rate25.sleep()


    def __framespublisher(self):
        while not self.__threadkiller:
            self.__createTransform("mav_" + str(self.__id) + "_home", "mav_" + str(self.__id) + "_startposition", self.__local_offset, self.__local_quat_offset, rospy.Time.now())
            self.__createTransform("mav_" + str(self.__id) + "_home", "mav_" + str(self.__id) + "_base_link", self.__local_pos, self.__local_quat, rospy.Time.now())
            self.__rate25.sleep()


    def __createTransform(self, parent_frame_id, child_frame_id, trans, rot, timestamp):
        t = TransformStamped()
        t.header.stamp = timestamp
        t.header.frame_id = parent_frame_id
        t.child_frame_id = child_frame_id
        t.transform.translation.x = trans[0]
        t.transform.translation.y = trans[1]
        t.transform.translation.z = trans[2]
        t.transform.rotation.x = rot[0]
        t.transform.rotation.y = rot[1]
        t.transform.rotation.z = rot[2]
        t.transform.rotation.w = rot[3]
        self._br.sendTransform(t)


    def lookupTransform(self, from_frame, to_frame):
        try:
            trans = self.__tfBuffer.lookup_transform(from_frame, to_frame, rospy.Time())
            return trans
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
            trans = False
            return trans
            pass

    def yawToNextGoto(self, setpoint_pos):
        dX = self.__local_pos[0] - setpoint_pos[0]
        dY = self.__local_pos[1] - setpoint_pos[1]
        yaw = math.atan2(dY, dX)
        yaw = yaw + math.pi/2
        return yaw


    def preflightcheck(self):
        self.logwarn("[PREFLIGHT-CHECK] ...")
        if not (self.__gpsFix):
            self.logerr("[GPS] ❌")
            return 0
        elif not (self.mavInSafetyZone()):
            return 0
        elif self.__extState.landed_state != 1:
            self.logerr("Not on ground!")
            return 0
        while not self.__offset:
            self.__setLocalOffset()
            self.__rate1.sleep()
        self.logwarn("[PREFLIGHT-CHECK] ✓")
        return 1


    def makeMavReadyForSetpoint(self):
        if not self.__gpsFix:
            self.logerr("[GPS] ❌")
        elif not self.__armed:
            self.arm()
        elif self.__extState.landed_state == 1:
            self.takeoff(2.5)
        elif self.__flightmode != "OFFBOARD":
            self.setFlightmode("OFFBOARD")
        elif self.mavInSafetyZone():
            return 1


    
    def mavInSafetyZone(self):
        return 1
        '''        
        if self.inSafetyZone(self.__local_pos):
            return 1
        else:
            self.logerr("Not in safety-zone!")
            return 0'''


    def inSafetyZone(self, pos):
        if not (min(self.__safety_x1, self.__safety_x2) <= pos[0] <= max(self.__safety_x1, self.__safety_x2)):
            return 1
        elif not (min(self.__safety_y1, self.__safety_y2) <= pos[1] <= max(self.__safety_y1, self.__safety_y2)):
            return 1
        elif not (pos[2] <= max(self.__safety_z1, self.__safety_z2)):
            return 1
        else:
            return 1


    def setSafetyZone(self, x1, y1, z1, x2, y2, z2):
        self.__safety_x1 = self.__local_offset_x + x1
        self.__safety_y1 = self.__local_offset_y + y1
        self.__safety_z1 = self.__local_offset_z + z1
        self.__safety_x2 = self.__local_offset_x + x2
        self.__safety_y2 = self.__local_offset_y + y2
        self.__safety_z2 = self.__local_offset_z + z2
        return 1


    def setFlightmode(self, flightmode):
        rospy.wait_for_service(self.__namespace + '/mavros/set_mode')
        try:
            setMode = rospy.ServiceProxy(self.__namespace + '/mavros/set_mode', SetMode)
            resp = setMode(0, flightmode)
            return 1
        except rospy.ServiceException, e:
            s = "Service call failed: %s"%e
            self.logerr(s)
            return 0

    
    def returnToHome(self):
        self.mavCMD(20)


    def reachedPos(self, pos, accuracy):
        if self.inSafetyZone(pos):
            des_pos = numpy.array(pos)
            cur_pos = numpy.array(self.__local_pos)
            if numpy.linalg.norm(des_pos - cur_pos) < accuracy:
                self.loginfo("[POSITION] ✓")
                return 1
        else:
            self.logerr("[POSITION] needs to be in safety zone.")
            return 0

    
    def reachedGoto(self):
        if self.reachedPos((self.__setpoint_x, self.__setpoint_y, self.__setpoint_z), self.__setpoint_accuracy):
            return 1
        else:
            return 0


    def reachedX(self, x, accuracy):
        if (x - accuracy < self.__local_x < x + accuracy):
            self.loginfo("[x-Position] reached ✓")
            return 1
        return 0


    def reachedY(self, y, accuracy):
        if (y - accuracy < self.__local_y < y + accuracy):
            self.loginfo("[y-Position] reached ✓")
            return 1
        return 0


    def reachedZ(self, z, accuracy):
        if (z - accuracy < self.__local_z < z + accuracy):
            self.loginfo("[z-Position] reached✓")
            return 1
        return 0


    def mavCMD(self, cmd_id, p1 = None, p2 = None, p3 = None, p4 = None, p5 = None, p6 = None, p7 = None):
        br = False
        confirmation = 0
        rospy.wait_for_service(self.__namespace + '/mavros/cmd/command')
        try:
            cmd = rospy.ServiceProxy(self.__namespace + '/mavros/cmd/command', CommandLong)
            cmd(br, cmd_id, confirmation, p1, p2, p3, p4, p5, p6, p7)
        except rospy.ServiceException, e:
            s = "Service call failed: %s"%e
            self.logerr(s)


    def arm(self):
        self.arming(True)


    def armed(self):
        return self.__armed


    def disarm(self):
        self.arming(False)


    def arming(self, boolean):
        if boolean == self.__armed:
            if boolean == True:
                self.loginfo("Already armed.")
            else:
                self.loginfo("Already disarmed.")
        else:
            if self.__gpsFix:
                rospy.wait_for_service(self.__namespace + '/mavros/cmd/arming')
                try:
                    arming = rospy.ServiceProxy(self.__namespace + '/mavros/cmd/arming', CommandBool)
                    respArm = arming(boolean)
                except rospy.ServiceException, e:
                    s = "Service call failed: %s"%e
                    self.logerr(s)
            else:
                self.logwarn("[GPS] ❌. Arming not possible!")


    def takeoff(self, altitude, yaw = 0.0):
        if (self.__gpsFix and self.__armed):
            rospy.wait_for_service(self.__namespace + '/mavros/cmd/takeoff')
            try:
                takeoff = rospy.ServiceProxy(self.__namespace + '/mavros/cmd/takeoff', CommandTOL)
                respTOL = takeoff(0, yaw, self.__latitude, self.__longitude, self.__altitude + altitude)
                self.loginfo("[TAKEOFF] " + str(altitude) + "m over ground.")
                ## not good because of stopping other process
                #while not self.reachedZ(altitude, 0.5):
                #    self.__setpoint_z = altitude
                #    self.__rate5.sleep()
                #return 1
            except rospy.ServiceException, e:
                s = "Service call failed: %s"%e
                self.logerr(s)
                return 0
        elif not self.__armed:
            self.logwarn("Not armed!")
            while not self.__armed:
                self.arm()
                self.__rate1.sleep()
            return 0
        else:
            self.logwarn("[GPS] ❌. Takeoff not possible")
            return 0
        

    def landed(self):
        if self.__extState.landed_state == 1:
            return 1
        else:
            return 0


    def land(self, altitude, yaw = 0.0):
        if (self.__gpsFix and self.__armed):
            rospy.wait_for_service(self.__namespace + '/mavros/cmd/land')
            try:
                land = rospy.ServiceProxy(self.__namespace + '/mavros/cmd/land', CommandTOL)
                respTOL = land(0, yaw, self.__latitude, self.__longitude, self.__altitude + altitude)
                self.loginfo("[LANDING] " + str(altitude) + "m over ground")
                return 1
            except rospy.ServiceException, e:
                s = "Service call failed: %s"%e
                self.logerr(s)
                return 0
        else:
            self.logwarn("[GPS] ❌. Landing not possible")
            return 0


    def goto(self, x, y, z, accuracy):
        self.__setpoint_accuracy = accuracy
        trans = False
        pos = [x, y, z]
        if self.inSafetyZone(pos):
            self.__goto = [x, y, z]
            timestamp = rospy.Time.now()
            self.__createTransform("mav_" + str(self.__id) + "_startposition", "mav_" + str(self.__id) + "_goto", self.__goto, self.__local_quat_offset, timestamp)
            while True:
                trans = self.lookupTransform("mav_" + str(self.__id) + "_home", "mav_" + str(self.__id) + "_goto")
                if trans:
                    diff_x = math.sqrt((self.__lastTransform.transform.translation.x - trans.transform.translation.x)**2)
                    diff_y = math.sqrt((self.__lastTransform.transform.translation.y - trans.transform.translation.y)**2)
                    diff_z = math.sqrt((self.__lastTransform.transform.translation.z - trans.transform.translation.z)**2)
                    if math.sqrt(diff_x**2 + diff_y**2 + diff_z**2) > accuracy:
                        break
                self.__createTransform("mav_" + str(self.__id) + "_startposition", "mav_" + str(self.__id) + "_goto", self.__goto, self.__local_quat_offset, timestamp)
                self.__rate25.sleep()
            self.__lastTransform = trans
            setpoint_pos = (trans.transform.translation.x, trans.transform.translation.y, trans.transform.translation.z)
            yaw = self.yawToNextGoto(setpoint_pos)
            self.setpoint_pos(setpoint_pos, yaw)
            self.loginfo("[GOTO] next [" + str(x) + "," + str(y) + "," + str(z) + "]")
            return 1
        else:
            self.logerr("[GOTO] ❌ . Needs to be in safety zone!")
            return 0


    def appendToQueue(self, x, y, z, accuracy = 0.5):
        self.__goto_queue.append((x, y, z, accuracy))
        return 1


    def clearQueue(self):
        self.__goto_queue = []
        return 1


    def setQueue(self, queue):
        self.__goto_queue = queue
        return 1

    
    def getQueue(self):
        return self.__goto_queue


    def startQueue(self, queueID = 0):
        self.__queue_active = True
  

    def stopQueue(self, queueID = 0):
        self.__queue_active = False


    def actualQueue(self):
        i = self.__queuePointer
        x = self.__goto_queue[i][0]
        y = self.__goto_queue[i][1]
        z = self.__goto_queue[i][2]
        accuracy = self.__goto_queue[i][3]
        self.goto(x, y, z, accuracy)
        if self.reachedGoto():
            self.__queuePointer = i+1


    def setpoint_pos(self, setpoint_pos, yaw = 0, accuracy = 1):
        if self.inSafetyZone(setpoint_pos): #and not self.__flightmode == "AUTO.RTL":
            self.__setpoint_mask = 3064 #4088
            self.__setpoint_x = setpoint_pos[0]
            self.__setpoint_y = setpoint_pos[1]
            self.__setpoint_z = setpoint_pos[2]
            self.__setpoint_yaw = yaw
            self.__setpoint_accuracy = accuracy
            while not self.makeMavReadyForSetpoint():
                self.__rate25.sleep()
        else:
            self.logerr("[SETPOINT] not in safety zone.")


    def setpoint_vel(self, vx, vy, vz = 0, z = 0, yaw = 0):
        if not self.__flightmode == "AUTO.RTL":
            if vz == 0:
                self.__setpoint_mask = 4035 ## vx, vy, z
            else:
                self.__setpoint_mask = 3015 ## vx, vy, vz, yaw
            self.__setpoint_vx = vx
            self.__setpoint_vy = vy
            self.__setpoint_vz = vz
            self.__setpoint_z = z
            self.__setpoint_yaw = yaw
            while not self.makeMavReadyForSetpoint() and not self.__flightmode == "AUTO.RTL":
                self.__rate25.sleep()



class queue():

    def __init__(self, *queueitem):
        self.queue = []
        for i in queueitem:
            self.queue.append(i)

    def __repr__(self):
        return repr(self.queue)

    def __del__(self):
        pass

    def append(self, queueitem):
        self.queue.append(queueitem)

    def add(self, queueposition, queueitem):
        self.queue[queueposition] = queueitem

    def get(self):
        return self.queue

    def clear(self):
        self.queue = []


class queueitem(queue):

    def __init__(self, x, y , z, accuracy):
        self.x = x
        self.y = y
        self.z = z
        self.accuracy = accuracy

    def __repr__(self):
        return repr([self.x, self.y, self.z, self.accuracy])


## -- WIP - THREADING
class setpoint(threading.Thread):
    def __init__(self, setpoint):
        threading.Thread.__init__(self)
        self.val = val
        self.__setpoint_mask = 3064 #4088
        self.__setpoint_x = 0.0
        self.__setpoint_y = 0.0
        self.__setpoint_z = 0.0
        self.__setpoint_vx = 0.0
        self.__setpoint_vy = 0.0
        self.__setpoint_vz = 0.0
        self.__setpoint_yaw = 0.0
        self.__setpoint_accuracy = 0.5

    def run(self):
        while not rospy.is_shutdown():
            rospy.Rate(5)

    def set_position(self, x, y, z):
        return 0
