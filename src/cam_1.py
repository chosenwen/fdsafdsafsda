#!/usr/bin/env python

# import #
import rospy
import numpy as np
import matplotlib.pyplot as plt
from sensor_msgs.msg import Image
from geometry_msgs.msg import PoseStamped 
from geometry_msgs.msg import Point
from std_msgs.msg import String
from std_msgs.msg import Bool
import cv2
from cv_bridge import CvBridge, CvBridgeError

# define global variable
r_pos = []
b_pos = []
g_pos = []
r_real_pos = []
b_real_pos = []
g_real_pos = []
old_num_red = 0 
old_num_green = 0
old_num_blue = 0
count_r = 0
count_g = 0
count_b = 0

current_z = 0 # current position
current_x = 0
current_y = 0

camera_state = '' # current camera state
stable_counter = 0 # stable counter

uav_state_red = True
uav_state_green = True
uav_state_blue = True
uav1_state = ''
uav2_state = ''
uav3_state = ''

rospy.init_node("uav1_sub", anonymous = True)
poseStamped = PoseStamped()
img = Image()
bridge = CvBridge()


def callback_image(img):
	global r_pos, b_pos, g_pos , r_real_pos, g_real_pos, b_real_pos
	global current_z, current_x, current_y, camera_state, stable_counter
	global old_num_red, old_num_green, old_num_blue
	global uav_state, uav_state_re
	# rospy.loginfo(uav_state)
	# rospy.loginfo('camera_state:' ,camera_state)
        rospy.loginfo("red target: %s", old_num_red)
        rospy.loginfo("green target: %s", old_num_green)
        rospy.loginfo("blue target: %s", old_num_blue)
        # rospy.loginfo('green target: ', old_num_green)
        # rospy.loginfo('green target: ', old_num_green)
	
	try:
		cv_image = bridge.imgmsg_to_cv2(img, "bgr8")
		( ori_h, ori_w) = cv_image.shape[:2]
		cv_image = cv2.resize(cv_image, ( ori_w/4, ori_h/4), interpolation=cv2.INTER_CUBIC)
		# print cv_image.shape[:2]
		# print cv_image.shape
	except CvBridgeError as e:
		print(e)
	
	# Convert BGR to HSV
	hsv = cv2.cvtColor(cv_image, cv2.COLOR_BGR2HSV)
	
	# define range of blue color in HSV
	lower_blue = np.array([110,50,50])
	upper_blue = np.array([130,255,255])
	# define range of green color in HSV
	lower_green = np.array([50, 100, 100])
	upper_green = np.array([70, 255, 255])
	# define range of red color in HSV
	lower_red = np.array([-10, 100, 100])
	upper_red = np.array([10, 255, 255])

	# detect and estimate object position
	if uav_state == "estimate_point": 
		# Threshold the HSV image to get blue, green, red colors
		mask_blue = cv2.inRange(hsv, lower_blue, upper_blue)
		mask_green = cv2.inRange(hsv, lower_green, upper_green)
		mask_red = cv2.inRange(hsv, lower_red, upper_red)
		# find contours and calculate the pixel coordinates of the box's center
		im2, contours_blue, hierarchy_blue = cv2.findContours(mask_blue, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
		im2, contours_red, hierarchy_red = cv2.findContours(mask_red, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
		im2, contours_green, hierarchy_green = cv2.findContours(mask_green, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
		# contour the objects
		cv2.drawContours(cv_image, contours_blue, -1,  (240, 230, 140), 3)
		cv2.drawContours(cv_image, contours_green, -1, (240, 230, 140), 3)
		cv2.drawContours(cv_image, contours_red, -1,   (240, 230, 140), 3)
		# count the number of each color object
		num_red = np.size(hierarchy_red, 1)
		num_green = np.size(hierarchy_green, 1)
		num_blue = np.size(hierarchy_blue, 1)
		# print "Red object: ", num_red
		# print "Green object: ", num_green
		# print "blue object: ", num_blue
	
		print "mom I'm in" 
		# calculate current ratio of real distance/ pixel
		#========================test=====================
		# current_z = 20 # !!!!!!!!!temperary
		# current_x = 22.46
		# current_y = -0.05
		#================================================
	
		ratio_x = current_z * 0.0765/20  # 0.0042
		ratio_y = current_z * 0.0759/20  # 0.0042
		print "z = ", current_z
		# print "ratio =",ratio
	
		# get position of each object in pixel
		for i in range(0, num_red): 
			r_pos.append(0) # add a space in the list
			r_pos[i] = 0 # initial the space !!important
			r_real_pos.append(0)
			r_real_pos[i] = 0
			for a in range(0, np.size(contours_red[i],1)): # in the number of point given by contours
			    r_pos[i] = r_pos[i] + contours_red[i][a]
			r_pos[i] = r_pos[i] / np.size(contours_red[i],1) 
			r_real_pos[i] = ((r_pos[i][0][0]-240)*ratio_x + current_x , (r_pos[i][0][1]-135)*ratio_y + current_y)
			print "red Nr.%d is at pixel"%(i), r_pos[i],", at realworld", r_real_pos[i]
			
		for i in range(0, num_green): 
			g_pos.append(0) # add a space in the list
			g_pos[i] = 0 # initial the space !!important
			g_real_pos.append(0)
			g_real_pos[i] = 0
			for a in range(0, np.size(contours_green[i],1)): # in the number of point given by contours
			    g_pos[i] = g_pos[i] + contours_green[i][a]
			g_pos[i] = g_pos[i] / np.size(contours_green[i],1)
			g_real_pos[i] = ((g_pos[i][0][0]-240)*ratio_x + current_x , (g_pos[i][0][1]-135)*ratio_y + current_y)
			print "green Nr.%d is at pixel"%(i), g_pos[i],", at realworld", g_real_pos[i]
	
		for i in range(0, num_blue): 
			b_pos.append(0) # add a space in the list
			b_pos[i] = 0 # initial the space !!important
			b_real_pos.append(0)
			b_real_pos[i] = 0
			for a in range(0, np.size(contours_blue[i],1)): # in the number of point given by contours
			    b_pos[i] = b_pos[i] + contours_blue[i][a]
			b_pos[i] = b_pos[i] / np.size(contours_blue[i],1)
			b_real_pos[i] = ((b_pos[i][0][0]-240)*ratio_x + current_x , (b_pos[i][0][1]-135)*ratio_y + current_y)
			print current_x, current_y
			print "blue Nr.%d is at pixel"%(i), b_pos[i],", at realworld", b_real_pos[i]
		if num_red == old_num_red and num_green == old_num_green and num_blue == old_num_blue :
			stable_counter = stable_counter + 1
		else:
	   		stable_counter = 0
		old_num_red = num_red
		old_num_green = num_green
		old_num_blue = num_blue
		print 'stable_counter = %i'%(stable_counter)
		if stable_counter > 100:
		 	camera_state = "estimate_end"
			pub_camera.publish(camera_state)
#===========================================================================================================
	if uav1_state == "gime_target" and uav_state_green == True: 
		nr_target_green = old_num_green-1
		target_green = Point()
		if nr_target_green > -1:
			target_green.x == r_real_pos[nr_target_green][0]
			target_green.y == r_real_pos[nr_target_green][1]
			pub_target_uav1.publish(target_green)
			camera_state = "given_green_target"
			pub_camera.publish(camera_state)
			old_num_green = old_num_green - 1
			uav_state_green = False

	elif uav1_state == "check":		
   		elif nr_target_green > -1:
    		mask_green = cv2.inRange(hsv, lower_green, upper_green)
    		im2, contours_green, hierarchy_green = cv2.findContours(mask_green, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    		cv2.drawContours(cv_image, contours_green, -1, (240, 230, 140), 3)
    		if (hierarchy_green != None):
    			M = cv2.moments(contours_green[0])
    			cX = int(M['m10']/M['m00'])
    			cY = int(M['m01']/M['m00'])
    			cv2.circle(cv_image, (cX, cY), 7, (0, 0, 0), -1)
    			print "cX, cY = %s, %s" %(cX, cY)
    			print compensation(cX,cY)	
    			camera_state = "given_compensation"
    			pub_camera.publish(camera_state)
    			pub_compensation_uav1.publish(compensation(cX, cY))
    	uav_state_green = True

	if uav2_state == "gime_target" and uav_state_red == True:
		nr_target_red = old_num_red-1
		target_red = Point()
		if nr_target_red > -1:
			target_red.x == r_real_pos[nr_target_red][0]
			target_red.y == r_real_pos[nr_target_red][1]
			pub_target_uav2.publish(target_red)
			camera_state = "given_red_target"
			pub_camera.publish(camera_state)
			old_num_red = old_num_red - 1
			uav_state_red = False

	elif uav2_state == "check":
		uav_state_red = True

	if uav3_state == "gime_target" and uav_state_blue == True:
		nr_target_blue = old_num_blue-1
		target_blue = Point()
		if nr_target_blue > -1:
			target_blue.x == r_real_pos[nr_target_blue][0]
			target_blue.y == r_real_pos[nr_target_blue][1]
			pub_target_uav3.publish(target_blue)
			camera_state = "given_blue_target"
			pub_camera.publish(camera_state)
			old_num_blue = old_num_blue - 1
			uav_state_blue = False

	elif uav3_state == "check":
		uav_state_blue = True

					
	cv2.imshow("image", cv_image)
	cv2.waitKey(3)

def callback_loc_pose(poseStamped):
	
	global current_z, current_x, current_y
	local_x = poseStamped.pose.position.x
	local_y = poseStamped.pose.position.y   
	local_z = poseStamped.pose.position.z
	
	current_z = local_z
	current_x = local_x
	current_y = local_y

def callback_checkbox_uav1(checkbox):
	global uav1_state
	uav1_state = checkbox.data

def callback_checkbox_uav2(checkbox):
	global uav2_state
	uav2_state = checkbox.data

def callback_checkbox_uav3(checkbox):
	global uav3_state
	uav3_state = checkbox.data

def compensation(cX, cY):
	if cY < 85:
		if cX < 190:
			direc = "ul"
		elif cX > 290:
			direc = "ur"
		else:
			direc = "u"
	elif cY > 185:
		if cX < 200:
			direc = "dl"
		elif cX > 290:
			direc = "dr"
		else:
			direc = "d"
	else:
		if cX < 200:
			direc = "l"
		elif cX > 290:
			direc = "r"
		else:
			direc = "down"
	return direc	
	
# Subscriber
rospy.Subscriber('/uav1/camera/image_raw', Image , callback_image)
rospy.Subscriber('/uav1/mavros/local_position/pose', PoseStamped , callback_loc_pose)
rospy.Subscriber('/uav1/checkbox_action', String, callback_checkbox_uav1)
rospy.Subscriber('/uav2/checkbox_action', String, callback_checkbox_uav2)
rospy.Subscriber('/uav3/checkbox_action', String, callback_checkbox_uav3)

pub_camera = rospy.Publisher('/uav1/camera_state', String, queue_size = 1)
left_no_object = rospy.Publisher('/left_no_object', Bool, queue_size = 1)
pub_target_uav1 = rospy.Publisher('/uav1/target', Point, queue_size = 1)
pub_target_uav2 = rospy.Publisher('/uav2/target', Point, queue_size = 1)
pub_target_uav3 = rospy.Publisher('/uav3/target', Point, queue_size = 1)
pub_compensation_uav1 = rospy.Publisher('/uav1/compensation', String, queue_size = 1)

r = rospy.Rate(25)
while not rospy.is_shutdown():
    r.sleep()
    


