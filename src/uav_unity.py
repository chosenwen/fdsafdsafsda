#!/usr/bin/env python

import smach
import sys
import rospy
import time
import tf
from mascor_px4_control import MAV
from geometry_msgs.msg import Point
from std_msgs.msg import String
from geometry_msgs.msg import PoseStamped

# create MAV object

def callback_pose_uav1(poseStamped):	
	global uav1_pose
	uav1_pose = poseStamped

def callback_pose_uav2(poseStamped):	
	global uav2_pose
	uav2_pose = poseStamped

def callback_pose_uav3(poseStamped):	
	global uav3_pose
	uav3_pose = poseStamped

# target callback function
def callback_target_uav1(point):
	global given_target_uav1
	given_target_uav1 = point

def callback_target_uav2(point):
	global given_target_uav2
	given_target_uav2 = point
	
def callback_target_uav3(point):
	global given_target_uav3
	given_target_uav3 = point

# camera state callback function
def callback_checkbox_camera1(checkbox):
	global camera1_state
	camera1_state = checkbox.data

def callback_checkbox_camera2(checkbox):
	global camera2_state
	camera2_state = checkbox.data

def callback_checkbox_camera3(checkbox):
	global camera3_state
	camera3_state = checkbox.data

# compensation callback function
def callback_compensation_camera1(data):
	global compensation_uav1
	compensation_uav1 = data.data

def callback_compensation_camera2(data):
	global compensation_uav2
	compensation_uav2 = data.data

def callback_compensation_camera3(data):
	global compensation_uav3
	compensation_uav3 = data.data

def pub_and_sub():
	# subscribe camera checkbox 
	rospy.Subscriber('/uav1/camera_state', String, callback_checkbox_camera1)
	rospy.Subscriber('/uav2/camera_state', String, callback_checkbox_camera2)
	rospy.Subscriber('/uav3/camera_state', String, callback_checkbox_camera3)
	# check compensation
	rospy.Subscriber('/uav1/compensation', String, callback_compensation_camera1)
	rospy.Subscriber('/uav2/compensation', String, callback_compensation_camera2)
	rospy.Subscriber('/uav3/compensation', String, callback_compensation_camera3)
	# save the position of each uav
	rospy.Subscriber('/uav1/mavros/local_position/pose', PoseStamped, callback_pose_uav1)
	rospy.Subscriber('/uav2/mavros/local_position/pose', PoseStamped, callback_pose_uav2)
	rospy.Subscriber('/uav3/mavros/local_position/pose', PoseStamped, callback_pose_uav3)
	# subscribe next target
	rospy.Subscriber('/uav1/target', Point, callback_target_uav1)
	rospy.Subscriber('/uav2/target', Point, callback_target_uav2)
	rospy.Subscriber('/uav3/target', Point, callback_target_uav3)

	# define Publisher
	pub_check_uav1 = rospy.Publisher('/uav1/checkbox_action', String, queue_size=10)
	pub_check_uav2 = rospy.Publisher('/uav2/checkbox_action', String, queue_size=10)
	pub_check_uav3 = rospy.Publisher('/uav3/checkbox_action', String, queue_size=10)


if __name__ == '__main__':
	pub_and_sub()
	# initial node
	rospy.init_node('UAV_control', anonymous=True)
	rate = rospy.Rate(25)
	listener = tf.TransformListener()
	
	# define UAV
	global multicopter1, multicopter2, multicopter3
	multicopter1 = MAV("/uav1", "multicopter")
	multicopter1.arm()
	# multicopter2 = MAV("/uav2", "multicopter")
	# multicopter2.arm()
	# multicopter3 = MAV("/uav3", "multicopter")
	# multicopter3.arm()

	global current_pos_uav1, current_pos_uav2, current_pos_uav3
	global given_target_uav1, given_target_uav2, given_target_uav3
	global camera1_state, camera2_state, camera3_state
	global compensation_uav1, compensation_uav2, compensation_uav3
	global local_target_uav1, local_target_uav2, local_target_uav3
	global stable_counter_uav1, stable_counter_uav2
	camera1_state = ''
	camera2_state = ''
	camera3_state = ''

	uav1_pose = PoseStamped()
	uav2_pose = PoseStamped()
	uav3_pose = PoseStamped()

	uav1_state = ''
	uav2_state = ''
	uav3_state = ''

	compensation_uav1 = ''
	compensation_uav2 = ''
	compensation_uav3 = ''

	given_target_uav1 = Point()
	given_target_uav2 = Point()
	given_target_uav3 = Point()
	
	local_target_uav1 = Point()
	local_target_uav2 = Point()
	local_target_uav3 = Point()

	stable_counter_uav1 = 0
	stable_cointer_uav2 = 0

	
	# Give init point
        local_target_uav1.x = 22.5
        local_target_uav1.y = -25
        local_target_uav1.z = 22
        
	rospy.loginfo('Executing state INIT') 
	multicopter1.setpoint_pos([local_target_uav1.x, local_target_uav1.y, local_target_uav1.z])
	uav1_state = "init"

	while not rospy.is_shutdown():
		print "x, y, z = %s, %s, %s" %(uav1_pose.pose.position.x, uav1_pose.pose.position.y, uav1_pose.pose.position.z)
		print uav1_state
		if uav1_state == "init":
			if (abs(abs(uav1_pose.pose.position.x)-abs(local_target_uav1.x)) < 0.5 and abs(abs(uav1_pose.pose.position.y)-abs(local_target_uav1.y)) < 0.5 and abs(abs(uav1_pose.pose.position.z)-abs(local_target_uav1.z)) < 0.5):
				stable_counter_uav1 = stable_counter_uav1 + 1
                print stable_counter_uav1
                if stable_counter_uav1 > 100:
			    	uav1_state = "estimate_point"
			    	pub_check_uav1.publish(uav1_state)
		if camera1_state == "estimate_end":
			uav1_state = "gime_target"
			pub_check_uav1.publish(uav1_state)
		elif camera1_state == "given_green_target":
			local_target_uav1.x = given_target_uav1.x
			local_target_uav1.y = given_target_uav1.y
			local_target_uav1.z = 8
			multicopter1.setpoint_pos([local_target_uav1.x, local_target_uav2.y, local_target_uav3.z])
			if (abs(abs(uav1_pose.pose.position.x)-abs(local_target_uav1.x)) < 1 and abs(abs(uav1_pose.pose.position.y)-abs(local_target_uav1.y)) < 1 and abs(abs(uav1_pose.pose.position.z)-abs(local_target_uav1.z)) < 1):
				uav1_state = "check"
				pub_checkaction.publish(uav1_state)
			else:
				print "haven't arrived"
		elif camera1_state == "given_compensation":
			print "Target: (%s, %s, %s)"%(local_target_uav1.x, local_target_uav1.y, local_target_uav1.z)
			#uav_state = "get the compensation"
			if uav1_state == "check":
				if uav1_pose.pose.position.z > 5:
					correction = 0.025
				elif uav1_pose.pose.position.z > 3:
					correction = 0.001

				if compensation_uav1 == "ul":
					local_target_uav1.x = local_target_uav1.x - correction 
					local_target_uav1.y = local_target_uav1.y + correction 
				elif compensation_uav1 == "ur":
					local_target_uav1.x = local_target_uav1.x + correction 
					local_target_uav1.y = local_target_uav1.y + correction 
				elif compensation_uav1 == "dl":
					local_target_uav1.x = local_target_uav1.x - correction 
					local_target_uav1.y = local_target_uav1.y - correction 
				elif compensation_uav1 == "dr":
					local_target_uav1.x = local_target_uav1.x + correction 
					local_target_uav1.y = local_target_uav1.y - correction 
				elif compensation_uav1 == "d":
					local_target_uav1.x = local_target_uav1.x
					local_target_uav1.y = local_target_uav1.y - correction 
				elif compensation_uav1 == "u":
					local_target_uav1.x = local_target_uav1.x
					local_target_uav1.y = local_target_uav1.y + correction 
				elif compensation_uav1 == "l":
					local_target_uav1.x = local_target_uav1.x - correction 
					local_target_uav1.y = local_target_uav1.y 
				elif compensation_uav1 == "r":
					local_target_uav1.x = local_target_uav1.x + correction 
					local_target_uav1.y = local_target_uav1.y
				elif compensation_uav1 == "down":
					if local_target_uav1.z > 3:
						local_target_uav1.z = local_target_uav1.z - 2
					else:
						#uav_state = "direct_go_down"
						local_target_uav1.z = 0.3
				multicopter1.setpoint_pos([local_target_uav1.x, local_target_uav1.y, local_target_uav1.z])
				if (abs(abs(uav1_pose.pose.position.x)-abs(local_target_uav1.x)) < 1 and abs(abs(uav1_pose.pose.position.y)-abs(local_target_uav1.y)) < 1 and abs(abs(uav1_pose.pose.position.z)-abs(local_target_uav1.z)) < 1):
					if uav1_pose.pose.position.z < 0.5:
						uav1_state = "go_drop"
			elif uav1_state == "go_drop":	
				local_target_uav1.z = 5
				local_target_uav1.x = -25.5
				local_target_uav1.y = -25
				multicopter1.setpoint_pos([local_target_uav1.x, local_target_uav1.y, local_target_uav1.z])
				if (abs(abs(uav1_pose.pose.position.x)-abs(local_target_uav1.x)) < 1 and abs(abs(uav1_pose.pose.position.y)-abs(local_target_uav1.y)) < 1 and abs(abs(uav1_pose.pose.position.z)-abs(local_target_uav1.z)) < 1):
					uav1_state = "drop"
			elif uav1_state == "drop":
				local_target.z = 0.5
				multicopter1.setpoint_pos([local_target.x, local_target.y, local_target.z])
				if (abs(abs(uav1_pose.pose.position.x)-abs(local_target_uav1.x)) < 1 and abs(abs(uav2_pose.position.y)-abs(local_target_uav1.y)) < 1 and abs(abs(uav1_pose.pose.position.z)-abs(local_target_uav1.z)) < 1):
					uav1_state = "gime_target"
			    	pub_checkaction.publish(uav1_state)
		rate.sleep()

