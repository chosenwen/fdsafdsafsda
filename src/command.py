#!/usr/bin/env python
import sys
import rospy
import time
import tf
from mascor_px4_control import MAV


def run_tests():
    rospy.init_node('mascorPX4controlTest', anonymous=True)
    rate = rospy.Rate(25)
    listener = tf.TransformListener()
    ## create MAV object
    multicopter1 = MAV("/uav1", "multicopter")
    multicopter1.arm()
    multicopter2 = MAV("/uav2", "multicopter")
    multicopter2.arm()
    multicopter3 = MAV("/uav3", "multicopter")
    multicopter3.arm()

    while not multicopter1.setFlightmode("OFFBOARD"):
        rate.sleep()
    while not multicopter2.setFlightmode("OFFBOARD"):
        rate.sleep()
    while not multicopter3.setFlightmode("OFFBOARD"):
        rate.sleep()

    multicopter1.setpoint_pos([20, -24, 20])
    multicopter2.setpoint_pos([20, 24, 20])
    multicopter3.setpoint_pos([20, 0, 20])


    while not rospy.is_shutdown():
        rate.sleep()
   
if __name__ == '__main__':

    try:
        run_tests()
    except rospy.ROSInterruptException:
        sys.exit() 
