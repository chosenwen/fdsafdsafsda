#!/usr/bin/env python

# import #
import rospy
import numpy as np
from sensor_msgs.msg import Image
from geometry_msgs.msg import PoseStamped
from std_msgs.msg import Float32 
from std_msgs.msg import String
import cv2
from cv_bridge import CvBridge, CvBridgeError

global cX, cY
cX = 0
cY = 0
global obj
obj = False

rospy.init_node("camsubscriber")
poseStamped = PoseStamped()
img = Image()
bridge = CvBridge()

position_x = Float32()
position_y = Float32()



def callback_image(img):
    global cX, cY
    try:
        cv_image = bridge.imgmsg_to_cv2(img, "bgr8")
        # print cv_image.shape # 1080 x 1920
    except CvBridgeError as e:
        print(e)

    # Convert BGR to HSV
    hsv = cv2.cvtColor(cv_image, cv2.COLOR_BGR2HSV)

    # define range of blue color in HSV
    lower_blue = np.array([110,50,50])
    upper_blue = np.array([130,255,255])

    # Threshold the HSV image to get only blue colors
    mask = cv2.inRange(hsv, lower_blue, upper_blue)
    
    # find contours and calculate the pixel coordinates of the box's center
    im2, contours, hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    
    obj = False
    if (hierarchy != None):
        M = cv2.moments(contours[0])
        cX = int(M['m10']/M['m00'])
        cY = int(M['m01']/M['m00'])
        cv2.circle(cv_image, (cX, cY), 7, (0, 0, 0), -1)
        obj = True


    # show the images
    cv2.imshow("Image window2", cv_image)
    cv2.waitKey(3)

def callback_loc_pose(poseStamped):

    local_x = poseStamped.pose.position.x
    local_y = poseStamped.pose.position.y   
    local_z = poseStamped.pose.position.z



rospy.Subscriber('/uav2/camera_right/image_raw', Image , callback_image)
rospy.Subscriber('/uav2/mavros/local_position/pose', PoseStamped , callback_loc_pose)

pub_dir = rospy.Publisher('/uav2/direction', String, queue_size=1)
# puby = rospy.Publisher('position_y', Float32, queue_size=1)


r = rospy.Rate(25)
direc = "stop"
while not rospy.is_shutdown():
    print(str(cX) + ',' + str(cY))
    # temp = cX
    # cX = cY
    # cY = temp
    if not obj:
        if cY < 490:
            if cX < 910:
                # up left coner
                direc = "ul"
            elif cX > 1010:
                # up right coner
                direc = "ur"
            else:
                # up 
                direc = "u"
        elif cY > 590:
            if cX < 910:
                # down left coner
                direc = "dl"
            elif cX > 1010:
                # down right coner
                direc = "dr"
            else:
                # down
                direc = "d"
        else:
            if cX < 910:
                # left coner
                direc = "l"
            elif cX > 1010:
                # right coner
                direc = "r"
    else:
        direc = "stop"
    # print direc
    pub_dir.publish(direc)
    r.sleep()










