#!/usr/bin/env python

# import #
import rospy
import numpy as np
import matplotlib.pyplot as plt
from sensor_msgs.msg import Image
from geometry_msgs.msg import PoseStamped 
from geometry_msgs.msg import Point
from std_msgs.msg import String
import cv2
from cv_bridge import CvBridge, CvBridgeError

# define global variable-----------------------------------

# define object point in image
r_pos = []
b_pos = []
g_pos = []
# define estimate position list
r_real_pos = []
b_real_pos = []
g_real_pos = []
# define amount of object with different color 
old_num_red = 0 
old_num_green = 0
old_num_blue = 0
# temp count
count_r = 0
count_g = 0
count_b = 0
# define current position
current_pose_uav1 = PoseStamped()
current_pose_uav2 = PoseStamped()
current_pose_uav3 = PoseStamped()
# define range of blue color in HSV
lower_blue = np.array([110,50,50])
upper_blue = np.array([130,255,255])
# define range of green color in HSV
lower_green = np.array([50, 100, 100])
upper_green = np.array([70, 255, 255])
# define range of red color in HSV
lower_red = np.array([-10, 100, 100])
upper_red = np.array([10, 255, 255])
# define states
camera_state_uav1 = ''
camera_state_uav2 = ''
camera_state_uav3 = ''
uav_state_uav1 = ''
uav_state_uav2 = ''
uav_state_uav3 = ''
#define image for each camera
img_uav1 = Image()
img_uav2 = Image()
img_uav3 = Image()


# define function---------------------------------------

# define subscriber
def camera_subscriber():
    rospy.Subscriber('/uav1/camera/image_raw', Image , callback_image_uav1) 
    rospy.Subscriber('/uav2/camera_right/image_raw', Image , callback_image_uav2)    
    rospy.Subscriber('/uav3/camera/image_raw', Image , callback_image_uav3) 

def uav_state_subscriber():
    rospy.Subscriber('/uav1/uav_state', String, callback_uav_state_uav1)
    # rospy.Subscriber('/uav2/uav_state', String, callback_uav_state_uav2)
    # rospy.Subscriber('/uav3/uav_state', String, callback_uav_state_uav3)

def uav_current_pose_subscriber():
    rospy.Subscriber('/uav1/mavros/local_position/pose', PoseStamped , callback_loc_pose_uav1)
    rospy.Subscriber('/uav2/mavros/local_position/pose', PoseStamped , callback_loc_pose_uav2)
    rospy.Subscriber('/uav3/mavros/local_position/pose', PoseStamped , callback_loc_pose_uav3)

# define publisher
def camera_state_publisher():
    pub_camera_uav1 = rospy.Publisher('/uav1/camera_state', String, queue_size = 1)
    pub_camera_uav2 = rospy.Publisher('/uav2/camera_state', String, queue_size = 1)
    pub_camera_uav3 = rospy.Publisher('/uav3/camera_state', String, queue_size = 1)

def target_publisher():
    pub_target_uav1 = rospy.Publisher('/uav1/target', Point, queue_size = 1)
    pub_target_uav1 = rospy.Publisher('/uav1/target', Point, queue_size = 1)
    pub_target_uav1 = rospy.Publisher('/uav1/target', Point, queue_size = 1)

# define callback function
def callback_image_uav1(img):
    global img_uav1
    img_uav1 = resize_image(img)
    cv2.namedWindow('image1', cv2.WINDOW_NORMAL)
    cv2.namedWindow('image2', cv2.WINDOW_NORMAL)
    cv2.namedWindow('image3', cv2.WINDOW_NORMAL)
    cv2.imshow("image1",img_uav1)
    cv2.imshow("image2",img_uav2)
    cv2.imshow("image3",img_uav3)
    cv2.waitKey(0)

def callback_image_uav2(img):
    global img_uav2
    img_uav2 = resize_image(img)
    

def callback_image_uav3(img):
    global img_uav3
    img_uav3 = resize_image(img)


def callback_uav_state_uav1(msg):
    global uav_state_uav1
    uav_state_uav1 = msg.data

def callback_uav_state_uav2(msg):
    global uav_state_uav2
    uav_state_uav2 = msg.data

def callback_uav_state_uav3(msg):
    global uav_state_uav3
    uav_state_uav3 = msg.data

def callback_loc_pose_uav1(posestamped):
    global current_pose_uav1
    current_pose_uav1 = posestamped

def callback_loc_pose_uav2(posestamped):
    global current_pose_uav2
    current_pose_uav2 = posestamped

def callback_loc_pose_uav3(posestamped):
    global current_pose_uav3
    current_pose_uav3 = posestamped

def resize_image(img):
    try:
	cv_image = bridge.imgmsg_to_cv2(img, "bgr8")
	( ori_h, ori_w) = cv_image.shape[:2]
	cv_image = cv2.resize(cv_image, ( ori_w/4, ori_h/4), interpolation=cv2.INTER_CUBIC)
    except CvBridgeError as e:
	print(e)
    return cv_image
   
def img_processing(img, color):
	# change image to hsv
	hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
	if color == 'red':
		# red mask
		mask = cv2.inRange(hsv, lower_red, upper_red)
		im2, contours, hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
		cv2.drawContours(img, contours, -1, (0, 0, 0), 3)
	
def estimate_obj(img, color):
	masked_img = img_processing(img, color)        















rospy.init_node("UAV_controller")

bridge = CvBridge()

# build up subscriber
camera_subscriber()
uav_state_subscriber()
uav_current_pose_subscriber()
# build up publisher
camera_state_publisher()
target_publisher()

rate = rospy.Rate(25)
while not rospy.is_shutdown():
	if camera_state_uav2 == '':	
		estimate_obj(uav_state_uav2,'red')
	rate.sleep()
cv2.destroyAllWindows()
